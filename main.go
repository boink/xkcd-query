// Copyright (C) 2023 by boink
// This program is free software: you can redistribute it and/or
// modify it under the terms of the Affero GNU General Public
// License as published by the Free Software Foundation, version 3
// of the License.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// Affero GNU General Public License for more details.

// You should have received a copy of the Affero GNU General Public
// License along with this program.  If not, see
// <https://www.gnu.org/licenses/>.
package main

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"math/rand"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"
)

const base = "https://xkcd.com"

type info struct {
	Num        int
	SafeTitle  string `json:"safe_title"`
	Transcript string
	Alt        string
	Img        string
}

var latestPostPath = "/info.0.json"

func usage(i int) {
	str := `Usage:
xkcd [-f FILE] [-d] [-p NUM | Search-Term...]
	Search xkcd's json FILE (default index.0.json) for Search-Term in transcript literally

	-f FILE
		Use FILE instead of default (index.0.json)
	-d
		Downlaod the json file from xkcd.com
	-p NUM
		Show post NUM instead of searching for the search term
	-h
		Show this help message`
	fmt.Println(str)
	os.Exit(i)
}

func main() {
	if len(os.Args) < 2 {
		usage(1)
	}
	var download bool
	var indexFile = "info.0.json"
	argsStart, transcript := 1, true
outfor:
	for i := 1; i < len(os.Args); i++ {
		switch os.Args[i] {
		case "-f":
			if len(os.Args) < i+2 {
				usage(1)
			}
			indexFile = os.Args[i+1]
			i += 1
		case "-d":
			download = true
		case "-p":
			transcript = false
		case "-h":
			usage(0)
		case "--":
			i+=1
			fallthrough
		default:
			break outfor // take everything after first non-option as search string
		}
		argsStart = i+1
	}

	decoder := setupDecoder(indexFile, download)
	var post info
	for decoder.More() {
		err := decoder.Decode(&post)
		checkerr(err)
		for _, v := range os.Args[argsStart:] {
			if transcript && strings.Contains(post.Transcript, v) {
				fmt.Println(post)
			} else if !transcript && v == strconv.Itoa(post.Num) {
				fmt.Println(post)
			}
		}
	}
}

func setupDecoder(indexFile string, download bool) *json.Decoder {
	xkcdFile, err := os.Open(indexFile)
	if os.IsNotExist(err) && download {
		initialSetup(indexFile)
		xkcdFile.Seek(0, io.SeekStart)
		err = nil
	}
	checkerr(err)
	decoder := json.NewDecoder(xkcdFile)
	return decoder
}

func initialSetup(indexFile string) {
	log.Println("Doing Initial Setup")
	tmpfile, err := os.CreateTemp("", "xkcd")
	checkerr(err)
	defer func() {
		tmpfile.Close()
		os.Remove(tmpfile.Name())
	}()
	writeJSON(base+latestPostPath, tmpfile)
	tmpfile.Seek(0, io.SeekStart)
	var post info
	err = json.NewDecoder(tmpfile).Decode(&post)
	checkerr(err)
	fmt.Println(&post)
	xkcdFile, err := os.Create(indexFile)
	checkerr(err)
	downloadPost(1, post.Num, xkcdFile) // post.Num will be the number of latest post
}

func checkerr(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func (post info) String() string {
	return fmt.Sprintf("Url: %s/%d\nImg: %s\nTranscript: %s",
		base, post.Num, post.Img, post.Transcript)
}

func writeJSON(url string, out io.Writer) error {
	resp, err := http.Get(url)
	if err != nil {
		return err
	}
	if r := resp.StatusCode; r%100 != 2 {
		return fmt.Errorf("%v", resp.Status)
	}
	defer resp.Body.Close()
	_, err = io.Copy(out, resp.Body)
	checkerr(err)
	return nil
}

func downloadPost(start, end int, xkcdFile io.Writer) {
	for i := start; i <= end; i++ {
		log.Println("Download post", i)
		err := writeJSON(fmt.Sprintf("%s/%d/info.0.json", base, i), xkcdFile)
		if err != nil {
			log.Printf("Skipping %d: %v\n", err)
		}
		time.Sleep(time.Duration(rand.Intn(10) * 1000))
	}
	log.Println("Download Finished")
}
